package com.example.MVCDivison.Service;

import com.example.MVCDivison.Model.DivisonModel;
import org.springframework.stereotype.Service;

@Service
public class DivisonService {
    double result;
    public double Divison(DivisonModel DM)
    {
        try{
         result= DM.getNum1()/ DM.getNum2();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
