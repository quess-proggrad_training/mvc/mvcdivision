package com.example.MVCDivison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcDivisonApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcDivisonApplication.class, args);
	}

}
