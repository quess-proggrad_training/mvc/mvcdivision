package com.example.MVCDivison.Controller;

import com.example.MVCDivison.Model.DivisonModel;
import com.example.MVCDivison.Service.DivisonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DivisonController {

    @Autowired
    DivisonService DS;
    @GetMapping("/divison")
    public String Control()
    {
        DivisonModel DM=new DivisonModel(10,0);
        double ans= DS.Divison(DM);
        return "Divison= "+ans;
    }
}
