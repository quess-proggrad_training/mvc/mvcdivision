package com.example.MVCDivison;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DivisonAspect {
    @Before("execution(*  com.example.MVCDivison.Controller.DivisonController.Control())")
    public void before()
    {
        System.out.println("Process Started");
    }
    @After("execution(*  com.example.MVCDivison.Controller.DivisonController.Control())")
    public void after()
    {
        System.out.println("Process Ended");
    }
}
